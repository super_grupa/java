package painting;

import java.awt.Color;
import java.awt.Graphics;

public class Kulka {
	private int r;
	private int xPos;
	private int yPos;
	private Color colour;

	public Kulka() {
		// TODO Auto-generated constructor stub
	}
	
	//gettery
	public int getr(){
        return r;
    }
	
	public int getX() {
		return xPos;
	}
	
	public int getY(){
        return yPos;
    }
	
	public Color getColor() {
		return colour;
	}

	//settery
	public void setr(int r){
        this.r = r;
    }
	
	public void setX(int xPos) {
		this.xPos = xPos;
	}
	
	public void setY(int yPos){
        this.yPos = yPos;
    }
	
	public void setColor(Color color){
        this.colour = color;
    }
	
	public void paint(Graphics g){
        g.setColor(colour);
        g.fillOval(xPos, yPos, r, r);
        
    }
	
}
