package painting;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.util.ArrayList;

import javax.swing.JPanel;

import java.util.List;
import java.util.Vector;

import javax.swing.JPanel;

import phisics_engine.Particle;
import user_interface.FrameGUI;

public class PaintPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<Kulka> kulki = new ArrayList<Kulka>();
	private double zoom = 2;

	public PaintPanel() {
		//domyslny konstruktor
	}
	
	public void addParticle(int x, int y, int r, Color c) {
		Kulka k = new Kulka();
		k.setColor(c);
		k.setr(r);
		k.setX(x);
		k.setY(y);
		kulki.add(k);
	}
	public void addParticle(Particle p) {
		Kulka k = new Kulka();
		if(p.get_charge() < 0) 
			k.setColor(Color.blue);
		else if(p.get_charge() > 0)
			k.setColor(Color.red);
		else 
			k.setColor(Color.gray);
		k.setr(Math.abs((int)p.get_mass() * 20) + 20);
		k.setX((int)(p.get_position().get_x() * Math.exp(zoom) + FrameGUI.panel_symulacji.getWidth()/2));
		k.setY((int)(p.get_position().get_y() * Math.exp(zoom) + FrameGUI.panel_symulacji.getHeight()/2));
		kulki.add(k);
	}
	public void update_kulki(Vector<Particle> p){
		kulki.clear();
		for (int i = 0; i < p.size(); i++) {
			addParticle(p.get(i));
		}
	}
	public void zoom_plus() {
		zoom += .25;
	}
	public void zoom_minus() {
		zoom -= .25;
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		for (int i = 0; i < kulki.size(); i++) {
			kulki.get(i).paint(g);
		}

	}
	

}
