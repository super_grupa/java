package user_interface;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.*;

import file_operations.Deserialize;
import file_operations.Serialize;
import painting.PaintPanel;
import phisics_engine.Particle;
import phisics_engine.Simulation;
import phisics_engine.SimulationData;
import phisics_engine.Vector3D;
import phisics_engine.fields.Magneticfield;

public class FrameGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//for menu
	JMenuBar menuBar;
		JMenu menu, languages;
		JMenuItem menuItem;
		JRadioButtonMenuItem rbMenuItem;
		JCheckBoxMenuItem cbMenuItem;
		JMenuItem zapispracy;
		JMenuItem wczytaniepracy;
		JMenuItem pytania;
		JButton tworcy;
		JCheckBoxMenuItem trybnocny;
		
	//panels with components
	JPanel gorny;
		JLabel tytul;
		JComboBox<String> wyborjezyka;
	JPanel dolny;
	JPanel lewy;
	JPanel prawy;
		JButton ustawpole;
		JButton ustawladunek;
		JLabel numberofparticles;
		JLabel numberoffields;
		JButton startstop;
	public static PaintPanel panel_symulacji; // jego sie zamieni pozniej JPanel -> Draw
	
	
	Simulation symulacja;
	SimulationData data;
	ExecutorService exec;
	
	public FrameGUI () {
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		data = new SimulationData(Deserialize.load_simulation("examples/blank.sav"));
		symulacja = new Simulation(data);
		
		
		
		
		
		
		//////////////////////////////////
		
		//MENU
				//super("JMenuDemo");
				setDefaultCloseOperation(DISPOSE_ON_CLOSE);
				
				//wiecej na http://docs.oracle.com/javase/tutorial/uiswing/components/menu.html
				
				JMenuBar menuBar;
				JMenu menu;
				
				//Create the menu bar.
				menuBar = new JMenuBar();

				//Build the first menu.
				menu = new JMenu("Menu");
				menu.setMnemonic(KeyEvent.VK_M);
				menuBar.add(menu);
				
				
				zapispracy = new JMenuItem("Zapisanie pracy");
				menu.add(zapispracy);
				ActionListener uchozapisu = new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						JFileChooser chooser = new JFileChooser();				
						//Otwarcie okienka; metoda blokuje się do czasu wybrania pliku lub
						//zamknięcia okna
						int result = chooser.showDialog(null, "Wybierz plik do zapisu");
						if (JFileChooser.APPROVE_OPTION == result){
						System.out.println("Wybrano plik: " +
						chooser.getSelectedFile());
						}else {
						System.out.println("Nie wybrano pliku");
						}
						
													
						//Tworzenie pliku zapisu w folderze projektu
						
						//File outputfile = chooser.getSelectedFile();
						
						//Zapis do pliku
						
						try {
							///////////////////////////////////////////////////////////////
							Serialize.save_simulation(symulacja.get_data(), chooser.getSelectedFile().getAbsolutePath());
							///////////////////////////////////////////////////////////////
							
						} catch (Exception e) {
							System.out.println(e.getMessage());
						}							
						
					}
					
					
				};
				zapispracy.addActionListener(uchozapisu);
				menu.addSeparator();
				wczytaniepracy = new JMenuItem("Wczytanie pracy");
				menu.add(wczytaniepracy);
				ActionListener uchowczytu = new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						JFileChooser chooser = new JFileChooser();				
						//Otwarcie okienka; metoda blokuje się do czasu wybrania pliku lub
						//zamknięcia okna
						int result = chooser.showDialog(null, "Wybierz plik do wczytania");
						if (JFileChooser.APPROVE_OPTION == result){
						System.out.println("Wybrano plik: " +
						chooser.getSelectedFile());
						}else {
						System.out.println("Nie wybrano pliku");
						}
						
													
						//Tworzenie pliku zapisu w folderze projektu
						
						//File outputfile = chooser.getSelectedFile();
						//Wczytanie
						try {
							///////////////////////////////////////////////////////////////
							
							symulacja = new Simulation(Deserialize.load_simulation(chooser.getSelectedFile().getAbsolutePath()));
							repaint();
							///////////////////////////////////////////////////////////////
						} catch (Exception e) {
							System.out.println(e.getMessage());
						}	
					}
					
					
				};
				wczytaniepracy.addActionListener(uchowczytu);
				
				menu.addSeparator();
				pytania = new JMenuItem("Najcześciej zadawane pytania");
				pytania.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent arg0) {
						Pytania pytanka = new Pytania();
						pytanka.setVisible(true);
					}
					
				});
				menu.add(pytania);	
				
				menu.addSeparator();
				trybnocny = new JCheckBoxMenuItem("Tryb nocny");
				menu.add(trybnocny);	
				
				menu.addSeparator();
				languages = new JMenu("Wybór języka");
				languages.setMnemonic(KeyEvent.VK_S);

				menuItem = new JMenuItem("Angielski");
				languages.add(menuItem);

				menuItem = new JMenuItem("Polski");
				languages.add(menuItem);
				menu.add(languages);
				
				menu.addSeparator();
				JMenuItem tworcy = new JMenuItem("Twórcy");
				tworcy.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent arg0) {
						JOptionPane.showMessageDialog(null, "Projekt Jana Rawy (305073) i Zuzanny Chochulskiej (305000)");
					}
					
				});
				menu.add(tworcy);
				
				menu.addSeparator();
				JMenuItem exit = new JMenuItem("Exit");
				exit.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent arg0) {
						exec.shutdown();
						System.exit(0);	
					}
					
				});
				menu.add(exit);		
				menuBar.add(menu);
						
				setJMenuBar(menuBar);
				
		//Panel gorny
				gorny = new JPanel();
				this.add(gorny, BorderLayout.PAGE_START); 
				gorny.setBackground(Color.GRAY);
				
				tytul = new JLabel("Ruch cząstki w polu - symulator");
				gorny.add(tytul);
		//Panel Symulacji
				panel_symulacji = new PaintPanel();
				this.add(panel_symulacji, BorderLayout.CENTER);
				//panel_symulacji.addParticle(100, 200, 50, Color.red);
				exec = Executors.newFixedThreadPool(2);
				exec.execute(symulacja);
				exec.execute(new Runnable() {

					@Override
					public void run() {
						while(true) {
							panel_symulacji.update_kulki(symulacja.get_data().get_particles());
							repaint();
							try {
								Thread.sleep(20);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						
					}
					
				});
				
				
		//Panel Prawy
				prawy = new JPanel();
				this.add(prawy, BorderLayout.LINE_END);
				prawy.setBackground(Color.LIGHT_GRAY);
				prawy.setLayout(new GridLayout(4,1));
				
				ustawpole = new JButton("Ustaw pole");
				prawy.add(ustawpole);
				ustawpole.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent arg0) {
						 JDialog d = new JDialog();
						 	d.setTitle("Ustaw pole!");
						 	
						 	JPanel infopanel = new JPanel();
						 	d.add(infopanel, BorderLayout.NORTH);
						 	JLabel info = new JLabel("All of the units are set in the SI metric system!");
						 	infopanel.add(info);
						 	
						    JPanel settingpanel = new JPanel();
						    d.add(settingpanel, BorderLayout.CENTER);
						    settingpanel.setBackground(Color.LIGHT_GRAY);
						    settingpanel.setLayout(new GridLayout(3,2));
						    JLabel fx = new JLabel("Force [x] component: ");
						    settingpanel.add(fx);
						    JTextField sfx = new JTextField("0");
						    settingpanel.add(sfx);
						    JLabel fy = new JLabel("Force [y] component: ");
						    settingpanel.add(fy);
						    JTextField sfy = new JTextField("0");
						    settingpanel.add(sfy);
						    JLabel fz = new JLabel("Force [z] component: ");
						    settingpanel.add(fz);
						    JTextField sfz = new JTextField("1");
						    settingpanel.add(sfz);
						    JPanel acceptingpanel = new JPanel();
						    d.add(acceptingpanel, BorderLayout.SOUTH);
						    JButton accept = new JButton("Submit");
						    accept.addActionListener(new ActionListener(){
								@Override
								public void actionPerformed(ActionEvent arg0) {
									double x_, y_, z_;
									try {
										x_ = Double.parseDouble(sfx.getText());
										y_ = Double.parseDouble(sfy.getText());
										z_ = Double.parseDouble(sfz.getText());
										Magneticfield mag = new Magneticfield(new Vector3D(x_, y_, z_));
										symulacja.get_data().get_fields().add(mag);
										numberoffields.setText(String.valueOf(symulacja.get_data().get_fields().size()));
									}
									catch (NumberFormatException e) {
										System.err.println("Wrong number format!");
									}
								}
								
							});
						    acceptingpanel.add(accept);
						    d.setSize(new Dimension(450, 300));
						    d.setResizable(false);
						    d.setVisible(true);
					}
					
				});
				
				ustawladunek = new JButton("Ustaw ładunek");
				prawy.add(ustawladunek);
				ustawladunek.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent arg0) {
						 JDialog d = new JDialog();
						 	d.setTitle("Set particle!");
						 	
						 	JPanel infopanel = new JPanel();
						 	d.add(infopanel, BorderLayout.NORTH);
						 	JLabel info = new JLabel("All of the units are set in the SI metric system!");
						 	infopanel.add(info);
						 	
						    JPanel settingpanel = new JPanel();
						    d.add(settingpanel, BorderLayout.CENTER);
						    settingpanel.setBackground(Color.LIGHT_GRAY);
						    settingpanel.setLayout(new GridLayout(8,2));
						    
						    
						    
						    JLabel mass = new JLabel("Mass of the particle: ");
						    settingpanel.add(mass);
						    JTextField smass = new JTextField("1");
						    settingpanel.add(smass);
						    JLabel charg = new JLabel("Charge of the particle: ");
						    settingpanel.add(charg);
						    JTextField scharg = new JTextField("1");
						    settingpanel.add(scharg);
						    
						    
						    
						    JLabel velox = new JLabel("Velocity [x] of the particle: ");
						    settingpanel.add(velox);
						    JTextField svelox = new JTextField("0");
						    settingpanel.add(svelox);
						    JLabel veloy = new JLabel("Velocity [y] of the particle: ");
						    settingpanel.add(veloy);
						    JTextField sveloy = new JTextField("0");
						    settingpanel.add(sveloy);
						    JLabel veloz = new JLabel("Velocity [z] of the particle: ");
						    settingpanel.add(veloz);
						    JTextField sveloz = new JTextField("0");
						    settingpanel.add(sveloz);
						    
						    
						    
						    JLabel posx = new JLabel("Position [x] of the particle: ");
						    settingpanel.add(posx);
						    JTextField sposx = new JTextField("0");
						    settingpanel.add(sposx);
						    JLabel posy = new JLabel("Position [y] of the particle: ");
						    settingpanel.add(posy);
						    JTextField sposy = new JTextField("0");
						    settingpanel.add(sposy);
						    JLabel posz = new JLabel("Position [z] of the particle: ");
						    settingpanel.add(posz);
						    JTextField sposz = new JTextField("0");
						    settingpanel.add(sposz);
						    
						    
						    JPanel acceptingpanel = new JPanel();
						    d.add(acceptingpanel, BorderLayout.SOUTH);
						    JButton accept = new JButton("Submit");
						    accept.addActionListener(new ActionListener(){
								@Override
								public void actionPerformed(ActionEvent arg0) {
									double rx_, ry_, rz_;
									double vx_, vy_, vz_;
									double m;
									int ch;
									try {
										ch = Integer.parseInt(scharg.getText());
										m = Double.parseDouble(smass.getText());
										
										vx_ = Double.parseDouble(svelox.getText());
										vy_ = Double.parseDouble(sveloy.getText());
										vz_ = Double.parseDouble(sveloz.getText());
										
										
										rx_ = Double.parseDouble(sposx.getText());
										ry_ = Double.parseDouble(sposy.getText());
										rz_ = Double.parseDouble(sposz.getText());
										Vector3D v = new Vector3D(vx_, vy_, vz_);
										Vector3D r = new Vector3D(rx_, ry_, rz_);
										symulacja.get_data().get_particles().add(new Particle(ch, m, v, r));
										numberofparticles.setText(String.valueOf(symulacja.get_data().get_particles().size()));
									}
									catch (NumberFormatException e) {
										System.err.println("Wrong number format!");
									}
								}
								
							});
						    acceptingpanel.add(accept);
						    d.setSize(new Dimension(450, 300));
						    d.setResizable(false);
						    d.setVisible(true);
					}
					
				});
				//panel for showing how many particles and fields we have
				JPanel aktualne = new JPanel();
				prawy.add(aktualne);
				aktualne.setBackground(Color.LIGHT_GRAY);
				aktualne.setBorder(BorderFactory.createLineBorder(Color.black));
				aktualne.setLayout(new GridLayout(2,2));
				//number of particles
				aktualne.add(new JLabel("Number of particles: "));
				numberofparticles = new JLabel(String.valueOf(symulacja.get_data().get_particles().size()));
				aktualne.add(numberofparticles);
				//number of fields
				aktualne.add(new JLabel("Number of fields: "));
				numberoffields = new JLabel(String.valueOf(symulacja.get_data().get_fields().size()));
				aktualne.add(numberoffields);
				
				
				startstop = new JButton("Start");
				prawy.add(startstop);
				ActionListener pauzowanie = new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						symulacja.un_pause_simulation();
					}
					
					
				};
				startstop.addActionListener(pauzowanie);
				
		//Panel dolny
				dolny = new JPanel();
				dolny.setBackground(Color.darkGray);
				this.add(dolny, BorderLayout.PAGE_END);
				
	}
	
	
	public static void main(String[] args) {
		FrameGUI ramka = new FrameGUI();
		ramka.setTitle("Symulacja cząstki w polu");
		ramka.setSize(1000,1600);
		//ramka.setBounds(100, 100, 900, 500);
		ramka.setVisible(true);
		
		
		
	}

}
