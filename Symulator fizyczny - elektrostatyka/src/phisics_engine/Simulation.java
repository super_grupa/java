package phisics_engine;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;
import phisics_engine.fields.Magneticfield;


public class Simulation implements Runnable{
	public Simulation(SimulationData simulation_data){
		data = simulation_data;
		time_step_counter = 0;
		p = false;
	}
	public void simulation() {
		for(int i = 0;i<data.get_particles().size();i++) {
			//v(t+delta_t/2)=v(t)+1/2*a(t)*delta_t
			data.get_particles().get(i).add_v(Vector3D.multiply_by_scalar(data.get_particles().get(i).get_acceleration(),time_step/2));
			//s(t+t/2)=s(t)+1/2*v(t+delta_t/2)*delta_t
			data.get_particles().get(i).add_r(Vector3D.multiply_by_scalar(data.get_particles().get(i).get_velocity(),time_step));
		}
			//a(t+delta_t)=F/m
			Frogleap.acceleration_calculator(data);
			
		for(int i = 0;i<data.get_particles().size();i++) {
			//v(t+delta_t)=v(t+delta_t/2)+1/2*a(t)*delta_t
			data.get_particles().get(i).add_v(Vector3D.multiply_by_scalar(data.get_particles().get(i).get_acceleration(),time_step/2));
		}
		time_step_counter++;
}
	public static double get_simulation_time() {
		return time_step*time_step_counter;
	}

	@Override
	public void run() {
		while(true) {
			while (p) {
				simulation();
				//System.out.println(get_simulation_time());
				try {
					Thread.sleep(2);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) {
		//Particle interaction test
		/*
		Vector3D v = new Vector3D();
		Vector3D r1 = new Vector3D(1,0,0);
		Vector3D r2 = new Vector3D(-1,0,0);
		Particle a = new Particle(-1,1,v,r1);
		Particle b = new Particle(-1,1,v,r2);
		Vector<Particle> czastki = new Vector<Particle>();
		czastki.add(a);
		czastki.add(b);
		simulation(czastki);
		simulation(czastki);
		simulation(czastki);
		simulation(czastki);
		*/
		//Field interaction test
		Vector3D v = new Vector3D(1, 0, 0);
		Vector3D r1 = new Vector3D();
		Magneticfield B = new Magneticfield(new Vector3D(0, 0, 1));
		
		
		Particle a = new Particle(-1,1,v,r1);
		Vector<Particle> czastki = new Vector<Particle>();
		czastki.add(a);
		SimulationData data = new SimulationData(czastki, B);
		
		try {
		      FileWriter myWriter = new FileWriter("/home/janraw/java/simulation_data.txt");
		      Simulation sym = new Simulation(data);
		      for(double t = 0; t<5; t+=time_step) {
		    	  sym.simulation();
		    	  myWriter.write(czastki.get(0).get_position().get_vector() + "\n");
		      }
		      
		      myWriter.close();
		      System.out.println("Successfully wrote to the file.");
		    } catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
		
		
	}
	public void un_pause_simulation() {
		if(p == true) p = false;
		if(p == false) p = true;
	}
	public Boolean get_state() {
		return p;
	}
	public SimulationData get_data() {
		return data;
	}
	public final static double time_step =  1.0e-3;
	private static long time_step_counter;
	private static Boolean p;
	private static SimulationData data;
}