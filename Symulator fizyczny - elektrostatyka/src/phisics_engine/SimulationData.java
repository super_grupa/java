package phisics_engine;

import java.io.Serializable;
import java.util.Vector;
import phisics_engine.fields.Field;

public class SimulationData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public SimulationData(Vector<Particle> p, Field f){
		particles = new Vector<Particle> (p);
		fields = new Vector<Field> ();
		fields.add(f);
	}
	public SimulationData(Vector<Particle> p, Vector<Field> f){
		particles = new Vector<Particle> (p);
		fields = new Vector<Field> (f);
	}
	public SimulationData(SimulationData a) {
		particles = new Vector<Particle> (a.get_particles());
		fields = new Vector<Field> (a.get_fields());
	}
	public SimulationData(Vector<Particle> p) {
		particles = new Vector<Particle> (p);
		fields = new Vector<Field> ();
	}
	public Vector<Particle> get_particles(){
		return particles;
	}
	public Vector<Field> get_fields(){
		return fields;
	}
	private Vector<Particle> particles;
	private Vector<Field> fields;
}
