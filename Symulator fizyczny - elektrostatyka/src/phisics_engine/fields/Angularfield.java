package phisics_engine.fields;

import phisics_engine.Particle;
import phisics_engine.Simulation;
import phisics_engine.Vector3D;

public class Angularfield implements Field{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public	Angularfield (double angular_frequency) {
		//x coordinate of magnetic field is sin(omega*t)
		B = new Vector3D(Math.sin(angular_frequency*Simulation.get_simulation_time()), 0, 0);
	}
	@Override
	public Vector3D particle_field_interaction(Particle a) {
		
		Vector3D intermediate = Vector3D.vector_product(a.get_velocity(), B);
		
		return Vector3D.multiply_by_scalar(intermediate, a.get_charge());
	}

	public Vector3D B;
}
